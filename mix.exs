defmodule Minimal.MixProject do
  use Mix.Project

  def project do
    [
      app: :gm,
      version: "0.1.1",
      erlc_options:
        [
          :debug_info,
          :warnings_as_errors,
          :warn_export_vars,
          :warn_shadow_vars,
          :warn_obsolete_guard,
        ] ++ erlc_options(Mix.env()),
      language: :erlang,
      deps: []
    ]
  end

  def application do
    []
  end

  defp erlc_options(:test), do: [:nowarn_export_all]
  defp erlc_options(_), do: []

end
